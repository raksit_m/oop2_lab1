package oop.lab1;

//TODO Write class Javadoc

/**
 * Some tests of Student behavior.
 * @author Raksit M.
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student, id is the id of the new Student.
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * Compare student's by id.
	 * They are equal if the id matches.
	 * @param other is another Object to compare to this one.
	 * @return true if the id is same and Object is Student, false otherwise.
	 */
	public boolean equals(Object other) {
		if(other.getClass() != Student.class) 
		return false;
		Student other2 = (Student) other;
		return this.id == other2.id;
	}
}